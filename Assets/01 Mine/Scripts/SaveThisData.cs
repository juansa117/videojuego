using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Syste.Runtime.Serialization.Formatters.Binary;

public class SaveThisData : MonoBehaviour
{
    public DataToBeSaved dTs;
    private object file;

    void Start()
    {
        LoadData();
    }

    public void SaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = null;
        if (!Directory.Exists(application.persistentDataPath + 
            "/SaveFolder"))
        {
            print("no extiste esta carpeta");
            Directory.CreateDirectory(Application.persistentDataPath +
                "/SaveFolder");
        }
        else
        {
            print("si existe esta carpeta");
        }
        if (file.Exists(Application.persistentDataPath + 
            "/SaveFolder/SaveData.dat"))
        {
            file = File.Open(Application.persistentDataPath + 
                "SaveFolder/SaveData.Dat, FileMode.Open");
        }
        else
        {
            file = File.Open(Application.persistentDataPath +
                "SaveFolder/SaveData.dat", FileMode.Create);
        }
        DataToBeSaved dtsvd = new DataToBeSaved(dTs);
        bf.Serialize(file, dtsvd);
        file.Close();
    }
    public void LoadData()
    {
        if (file.Exists(Application.persistentDataPath + "/SaveFolder/SaveData.dat")) ;
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(application.persistentDataPath +
                "/SaveFolder/SaveData.dat", FileMode.Open);
            dTs = new DataToBeSaved((DataToBeSaved)bf.Deserialize(file));
        }
    }

    [System.Serializable]

    public class DataToBeSaved
    {
        public string characterName;
        public int life;
        public int money;
        public int spawnZone;
        public List<bool> items;
        public float Health;
        public DataToBeSaved(DataToBeSaved dts)
        {
            characterName = dts.characterName;
            life = dts.life;
            money = dts.money;
            spawnZone = dts.spawnZone;
            items = dts.items;
            Health = dts.Health;
        }
    }