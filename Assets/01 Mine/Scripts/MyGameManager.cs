using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MyGameManager : MonoBehaviour
{
    static MyGameManager totalGameManager;
    void Awake()
    {
        if(totalGameManager != this)
        {
            Destroy(this.gameObject);
        }
        totalGameManager = this;
        DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoadThisScene (string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
